//
//  ViewController.swift
//  Calculator
//
//  Created by Chhaileng Peng on 6/2/18.
//  Copyright © 2018 Chhaileng Peng. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    
    @IBOutlet weak var lblResult: UILabel!
    @IBOutlet weak var lblOperation: UILabel!
    
    var previousNumber: Double = 0.0
    var currentNumber: Double = 0.0
    var opera: String = ""
    var performMath: Bool = false
    var sign = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    

    @IBAction func btnNumber_Pressed(_ sender: UIButton) {
        
       
        if lblResult.text == "0" && sender.tag != 10{
            lblResult.text = ""
        }
 
        switch sender.tag {
            case 0,1,2,3,4,5,6,7,8,9:
                lblResult.text! += String(sender.tag)
            case 10:
                if lblResult.text!.range(of:".") != nil {
                    return
                }
                lblResult.text! += "."
            case 11:
                
                let val = Double(lblResult.text!)
                if val != 0 {
                    if sign {
                        lblResult.text = String(fabs(val!))
                        sign = false
                    }else{
                        lblResult.text = "-" +  String(fabs(val!))
                        sign = true
                    }
                }
            default:
            return
        }
        
        currentNumber = Double(lblResult.text!)!
    }
    
    
    @IBAction func btnCancel_Pressed(_ sender: UIButton) {
        
        lblResult.text = "0"
        lblOperation.text = ""
        previousNumber = 0
        currentNumber = 0
        opera = ""
        performMath = false
        
    }
    
    @IBAction func btnOperator_Pressed(_ sender: UIButton) {
        
        var result: Double = 0
        switch  sender.titleLabel!.text {
            case "+" :
                if performMath {

                    result = performOperation(previousNumber: previousNumber, currentNumber: currentNumber, opera: opera)
                    previousNumber = result
                    lblOperation.text =  customNumberFormat(val: result) + " +"

                }else{
                    previousNumber = currentNumber
                    lblOperation.text = lblResult.text! + " +"

                }
                opera = "+"


            case "-" :
                if performMath {

                    result = performOperation(previousNumber: previousNumber, currentNumber: currentNumber, opera: opera)
                    previousNumber = result
                    lblOperation.text =  customNumberFormat(val: result) + " -"
                    
                }else{
                    previousNumber = currentNumber
                    lblOperation.text = lblResult.text! + " -"
                    
                }
                opera = "-"

            case "×" :
                if performMath {

                    result = performOperation(previousNumber: previousNumber, currentNumber: currentNumber, opera: opera)
                    previousNumber = result
                    lblOperation.text =  customNumberFormat(val: result) + " ×"

                }else{
                    previousNumber = currentNumber
                    lblOperation.text = lblResult.text! + " ×"

                }
                opera = "*"

            case "÷" :
                if performMath {

                    result = performOperation(previousNumber: previousNumber, currentNumber: currentNumber, opera: opera)
                    previousNumber = result
                    lblOperation.text =  customNumberFormat(val: result) + " ÷"
                }else{
                    previousNumber = currentNumber
                    lblOperation.text = lblResult.text! + " ÷"

                }
                opera = "/"

            
            case "%" :
                if performMath {
                    
                    result = performOperation(previousNumber: previousNumber, currentNumber: currentNumber, opera: opera)
                    previousNumber = result
                    lblOperation.text =  String(previousNumber) + " %"
                    
                }else{
                    previousNumber = currentNumber
                    lblOperation.text = lblResult.text! + " %"
                    
                }
                opera = "%"

            
            case "=" :
                
                if performMath {
                
                    result = performOperation(previousNumber: previousNumber, currentNumber: currentNumber, opera: opera)
                    lblResult.text =  customNumberFormat(val: result)
                    
                    if opera == "*" {
                        lblOperation.text = customNumberFormat(val: previousNumber) + " × " +  customNumberFormat(val: currentNumber)
                    }else if opera == "/" {
                        lblOperation.text = customNumberFormat(val: previousNumber) + " ÷ " +  customNumberFormat(val: currentNumber)
                    }else {
                        lblOperation.text = customNumberFormat(val: previousNumber) + " " + opera + " " +  customNumberFormat(val: currentNumber)
                    }
 
                }else{
                    
                    
                    lblOperation.text = customNumberFormat(val: Double(String(lblResult.text!))!)
                }
                return
            default:
                return
        }
        lblResult.text = "0"
        performMath = true

  
        
//        switch sender.titleLabel!.text {
//            case "+" :
//
//            case "-" :
//                 print("-")
//            case "×" :
//                 print("*")
//            case "÷" :
//                print("/")
//            case "=" :
//
//            default:
//                return
//        }
//        lblResult.text = "0"
//        performMath = true
//
//        print(performMath)
    }
    
    
    
    func  performOperation(previousNumber: Double, currentNumber: Double, opera: String ) -> Double {
      
        switch  opera {
            
            case "+":
                return previousNumber + currentNumber
            case "-":
                return previousNumber - currentNumber
            case "*":
                return previousNumber * currentNumber
            case "/":
                return previousNumber / currentNumber
            case "%":
                return previousNumber.truncatingRemainder(dividingBy: currentNumber)
            default:
                return 0.0;
        }
    }
    

    
    func customNumberFormat(val: Double) -> String {
        let tempVar = String(format: "%g", val)
        return tempVar
    }
    
}

